capabilities; and if it does not assume the personality and mentality of a white
dragon, it will know what it formerly knew as well. Another example: an 8th
level fighter successfully polymorphed into a blue dragon would know combat
with weapons and be able to employ them with prehensile dragon forepaws if
the fighter did not take on dragon personality and mentality. However, the
new form of the polymorphed creature may be stronger than it looks, i.e. a
mummy changed to a puppy dog would be very tough, or a brontosaurus
changed to an ant would be impossible to squash merely from being stepped
on by a small creature or even a man-sized one. The magic-user must use a
dispel magic spell to change the polymorphed creature back to its original
form, and this too requires a “system shock” saving throw. The material
component of this spell is a caterpillar cocoon.
Polymorph Self (Alteration)
Level: 4 Components: V
Range: 0 Casting Time: 3 segments
Duration: 2 turns/level Saving Throw: None
Area of Effect: The magic-user
Explanation/Description: When this spell is cast, the magic-user is able to
assume the form of any creature — from as small as a wren to as large as a
hippopotamus — and its form of locomotion as well. The spell does not give the
other abilities (attack, magic, etc.), nor does it run the risk of changing
personality and mentality. No “system shock” check is required. Thus, a magicuser
changed to an owl could fly, but his or her vision would be human; a
change to a black pudding would enable movement under doors or along halls
and ceilings, but not the pudding’s offensive or defensive capabilities. Naturally,
the strength of the new form must be sufficient to allow normal movement. The
spell caster can change his or her form as often as desired, the change
requiring only 5 segments. Damage to the polymorphed form is computed as if
it were inflicted upon the magic-user, but when the magic-user returns to his or
her own form, from 1 to 12 (d12) points of damage are restored.
