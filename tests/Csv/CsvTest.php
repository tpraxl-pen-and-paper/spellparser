<?php

namespace Tests\Csv;

use SpellParser\Csv\Csv;
use SpellParser\Csv\LineWriter;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributeNames;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributes;
use SpellParser\SpellParser\Spell\Heading\SpellHeading;
use SpellParser\Csv\SpellRowRenderer;
use Tests\TestCase;

class CsvTest extends TestCase
{
    /** @test */
    public function it_renders_an_empty_string_for_an_empty_array()
    {
        $rows = [];
        $this->assertContentRetrieverResult(
            "",
            function ($lineWriter) use ($rows) {
                foreach ($rows as $row) {
                    $lineWriter->append($row);
                }
            }
        );
    }

    /** @test */
    public function it_renders_a_proper_csv()
    {
        $rows = [
            ["row1-col1", "row1-col2"],
            ["row2-col1", "row2-col2"]
        ];
        $this->assertContentRetrieverResult(
            "row1-col1,row1-col2\n".
            "row2-col1,row2-col2\n",
            function ($lineWriter) use ($rows) {
                foreach ($rows as $row) {
                    $lineWriter->append($row);
                }
            }
        );
    }

    private function assertContentRetrieverResult($expected, callable $contentRetriever)
    {
        $csv = new Csv();
        $result = $csv->toString($contentRetriever);
        $this->assertEquals($expected, $result);
    }

    /** @test */
    public function it_saves_to_proper_file()
    {
        $csv = new Csv();
        $filename = tempnam(sys_get_temp_dir(), 'test-spellparser-');
        $csv->toFile($filename, function (LineWriter $lineWriter) {
            $lineWriter->append(['row1-1', 'row1-2']);
        });
        $this->assertEquals("row1-1,row1-2\n", file_get_contents($filename));
        unlink($filename);
    }

    /**
     * @test
     * @dataProvider provideSpellConfigs
     */
    public function it_can_be_used_to_render_spells($config, $expected)
    {
        $renderers = array_map(function ($config) {
            $heading = $config['heading'];
            $attr = $config['attributes'];
            $explanation = $config['explanation'];
            return new SpellRowRenderer(
                $config['class'],
                new SpellHeading($heading['title'], $heading['school'], $heading['reversible']),
                new SpellAttributes(
                    $attr[SpellAttributeNames::LEVEL],
                    $attr[SpellAttributeNames::COMPONENTS],
                    $attr[SpellAttributeNames::RANGE],
                    $attr[SpellAttributeNames::CASTING_TIME],
                    $attr[SpellAttributeNames::DURATION],
                    $attr[SpellAttributeNames::SAVING_THROW],
                    $attr[SpellAttributeNames::AREA_OF_EFFECT]
                ),
                $explanation
            );
        }, $config);
        $this->assertContentRetrieverResult(
            $expected,
            function (LineWriter $lineWriter) use ($renderers) {
                foreach ($renderers as $renderer) {
                    $lineWriter->append($renderer->renderArray());
                }
            }
        );
    }

    public function provideSpellConfigs()
    {
        return [
            '2-spells' => [
                [
                    [
                        'class' => 'DRUID',
                        'heading' => [
                            'title' => 'spell-title',
                            'school' => 'school',
                            'reversible' => true
                        ],
                        'attributes' => [
                            SpellAttributeNames::LEVEL => 1,
                            SpellAttributeNames::COMPONENTS => 'M, V',
                            SpellAttributeNames::RANGE => 'special range',
                            SpellAttributeNames::CASTING_TIME => 'special time',
                            SpellAttributeNames::DURATION => 'special duration',
                            SpellAttributeNames::SAVING_THROW => 'none',
                            SpellAttributeNames::AREA_OF_EFFECT => 'special area of effect'
                        ],
                        'explanation' => 'explanation'
                    ],
                    [
                        'class' => 'DRUID',
                        'heading' => [
                            'title' => 'spell-title-2',
                            'school' => 'school-2',
                            'reversible' => false
                        ],
                        'attributes' => [
                            SpellAttributeNames::LEVEL => 2,
                            SpellAttributeNames::COMPONENTS => 'S',
                            SpellAttributeNames::RANGE => '1 foot',
                            SpellAttributeNames::CASTING_TIME => '1 segment',
                            SpellAttributeNames::DURATION => '1 turn',
                            SpellAttributeNames::SAVING_THROW => 'none',
                            SpellAttributeNames::AREA_OF_EFFECT => "1''"
                        ],
                        'explanation' => 'explanation-2'
                    ]
                ],
                'DRUID,spell-title,school,reversible,1,"M, V","special range","special time","special duration",none,"special area of effect",explanation' . "\n" .
                'DRUID,spell-title-2,school-2,,2,S,"1 foot","1 segment","1 turn",none,1\'\',explanation-2' . "\n"
            ]
        ];
    }
}
