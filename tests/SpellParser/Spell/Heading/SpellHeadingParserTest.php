<?php

namespace Tests\SpellParser\Spell\Heading;

use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spell\Heading\SpellHeadingParseException;
use SpellParser\SpellParser\Spell\Heading\SpellHeadingParser;
use Tests\TestCase;

class SpellHeadingParserTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideSpellHeadings
     */
    public function it_extracts_the_actual_title_school_and_reversible_attribute($input, $expectedTitle, $expectedSchool, $expectedReversibleAttribute)
    {
        $spellHeading = (new SpellHeadingParser($input))->parseHeading(new Factory());
        $this->assertEquals($expectedTitle, $spellHeading->getTitle());
        $this->assertEquals($expectedSchool, $spellHeading->getSchool());
        $this->assertEquals($expectedReversibleAttribute, $spellHeading->isReversible());
    }

    public function provideSpellHeadings(): array
    {
        return [
            'Telekinesis (Alteration)' => ['Telekinesis (Alteration)', 'Telekinesis', 'Alteration', false],
            'Bless (Conjuration/Summoning) Reversible' => ['Bless (Conjuration/Summoning) Reversible', 'Bless', 'Conjuration/Summoning', true],
            'Cure Light Wounds (Necromantic) Reversible' => ['Cure Light Wounds (Necromantic) Reversible', 'Cure Light Wounds', 'Necromantic', true],
            'Project Image (Alteration, Illusion/Phantasm)' => ['Project Image (Alteration, Illusion/Phantasm)', 'Project Image', 'Alteration, Illusion/Phantasm', false]
        ];
    }

    /** @test */
    public function it_throws_an_exception_if_the_search_pattern_does_not_match()
    {
        $input = "MAGIC-USER SPELLS";
        try {
            (new SpellHeadingParser($input))->parseHeading(new Factory());
            $this->fail("Failed expecting that the unparsable string '${input}' causes an Exception to be thrown");
        } catch (SpellHeadingParseException $e) {
            $this->assertTrue(true);
        }
    }
}
