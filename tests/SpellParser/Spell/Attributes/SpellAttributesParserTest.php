<?php

namespace Tests\SpellParser\Spell\Attributes;

use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributeNames;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributesParser;
use Tests\TestCase;

class SpellAttributesParserTest extends TestCase
{
    /**
     * @test
     */
    public function it_parses_the_expected_values()
    {
        $level = '1';
        $components = 'V, S, M';
        $range = '8”';
        $castingTime = '3 segments';
        $duration = '1 turn';
        $savingThrow = 'Special';
        $areaOfEffect = '4” diameter';
        $attributes = join('', [
            SpellAttributeNames::LEVEL, ":  ${level}",
            SpellAttributeNames::COMPONENTS, ": ${components}\n",
            SpellAttributeNames::RANGE, ": ${range}",
            SpellAttributeNames::CASTING_TIME, ": ${castingTime}\n",
            SpellAttributeNames::DURATION, ": ${duration}",
            SpellAttributeNames::SAVING_THROW, ": ${savingThrow}\n",
            SpellAttributeNames::AREA_OF_EFFECT, ": ${areaOfEffect}\n"
        ]);
        $attributes = (new SpellAttributesParser($attributes))->parseAttributes(new Factory());
        $this->assertEquals($level, $attributes->getLevel());
        $this->assertEquals($components, $attributes->getComponents());
        $this->assertEquals($range, $attributes->getRange());
        $this->assertEquals($castingTime, $attributes->getCastingTime());
        $this->assertEquals($duration, $attributes->getDuration());
        $this->assertEquals($savingThrow, $attributes->getSavingThrow());
        $this->assertEquals($areaOfEffect, $attributes->getAreaOfEffect());
    }

    /**
     * @test
     * @dataProvider provideAttributesWithMissingValues
     */
    public function it_handles_missing_values($stringToParse, $level, $components, $range, $castingTime, $duration, $savingThrow, $areaOfEffect)
    {
        $attributes = (new SpellAttributesParser($stringToParse))->parseAttributes(new Factory());
        $this->assertEquals($level, $attributes->getLevel());
        $this->assertEquals($components, $attributes->getComponents());
        $this->assertEquals($range, $attributes->getRange());
        $this->assertEquals($castingTime, $attributes->getCastingTime());
        $this->assertEquals($duration, $attributes->getDuration());
        $this->assertEquals($savingThrow, $attributes->getSavingThrow());
        $this->assertEquals($areaOfEffect, $attributes->getAreaOfEffect());
    }

    public function provideAttributesWithMissingValues()
    {
        return [
            'missing level' => $this->getTestdata([
                SpellAttributeNames::COMPONENTS => 'V, M',
                SpellAttributeNames::RANGE => '4”',
                SpellAttributeNames::CASTING_TIME => 'Special',
                SpellAttributeNames::DURATION => '1 turn + 1/2 turn per level',
                SpellAttributeNames::SAVING_THROW => 'None',
                SpellAttributeNames::AREA_OF_EFFECT => '2” diameter'
            ]),
            'missing component' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '2',
                SpellAttributeNames::RANGE => '4” per level',
                SpellAttributeNames::CASTING_TIME => '1 segment',
                SpellAttributeNames::DURATION => '1 turn + 1/2 turn per level',
                SpellAttributeNames::SAVING_THROW => 'Special',
                SpellAttributeNames::AREA_OF_EFFECT => '4” diameter globe'
            ]),
            'missing range' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '3',
                SpellAttributeNames::COMPONENTS => 'V',
                SpellAttributeNames::CASTING_TIME => 'None',
                SpellAttributeNames::DURATION => '1 turn',
                SpellAttributeNames::SAVING_THROW => 'None',
                SpellAttributeNames::AREA_OF_EFFECT => 'Special'
            ]),
            'missing casting time' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '4',
                SpellAttributeNames::COMPONENTS => 'V, M, S',
                SpellAttributeNames::RANGE => '3d6 feet',
                SpellAttributeNames::DURATION => '1 turn + 1/2 turn per level',
                SpellAttributeNames::SAVING_THROW => 'None',
                SpellAttributeNames::AREA_OF_EFFECT => '3 Creatures'
            ]),
            'missing duration' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '5',
                SpellAttributeNames::COMPONENTS => 'M',
                SpellAttributeNames::RANGE => 'Special',
                SpellAttributeNames::CASTING_TIME => 'Special',
                SpellAttributeNames::SAVING_THROW => 'None',
                SpellAttributeNames::AREA_OF_EFFECT => '2” diameter'
            ]),
            'missing saving throw' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '6',
                SpellAttributeNames::COMPONENTS => 'S',
                SpellAttributeNames::RANGE => 'Touch',
                SpellAttributeNames::CASTING_TIME => 'Special',
                SpellAttributeNames::DURATION => '1 turn + 1/2 turn per level',
                SpellAttributeNames::AREA_OF_EFFECT => '2” diameter'
            ]),
            'missing area of effect' => $this->getTestdata([
                SpellAttributeNames::LEVEL => '7',
                SpellAttributeNames::COMPONENTS => 'V, S',
                SpellAttributeNames::RANGE => '4”',
                SpellAttributeNames::CASTING_TIME => 'Special',
                SpellAttributeNames::DURATION => '1 turn + 1/2 turn per level',
                SpellAttributeNames::SAVING_THROW => 'None'
            ]),


        ];
    }

    private function getTestdata(array $array)
    {
        // Create string of key: value (e.g. Level: 1)
        $mapped = array_map(fn ($key, $value) => "${key}: ${value}", array_keys($array), array_values($array));
        // segregate the array: 0, 1, 2, 3 -> [0, 1], [2, 3]
        $chunks = array_chunk($mapped, 2);
        // join the chunks. (E.g. Level: 1 Components: V)
        $pairs = array_map(
            fn ($chunk) => join(' ', $chunk),
            $chunks
        );
        // join the lines
        $stringToParse = join("\n", $pairs);
        // provide testSet
        return [
            $stringToParse,
            $array[SpellAttributeNames::LEVEL] ?? null,
            $array[SpellAttributeNames::COMPONENTS] ?? null,
            $array[SpellAttributeNames::RANGE] ?? null,
            $array[SpellAttributeNames::CASTING_TIME] ?? null,
            $array[SpellAttributeNames::DURATION] ?? null,
            $array[SpellAttributeNames::SAVING_THROW] ?? null,
            $array[SpellAttributeNames::AREA_OF_EFFECT] ?? null,
        ];
    }
}
