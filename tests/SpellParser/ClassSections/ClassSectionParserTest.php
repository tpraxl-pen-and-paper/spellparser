<?php

namespace Tests\SpellParser\ClassSections;

use SpellParser\SpellParser\ClassSections\ClassSection;
use SpellParser\SpellParser\ClassSections\ClassSectionParser;
use Tests\TestCase;

class ClassSectionParserTest extends TestCase
{
    private $druidSpellsPath = 'class-sections/druid-spells.txt';
    private $allClassesPath = 'class-sections/all-classes-with-shortened-content.txt';

    /** @test */
    public function it_finds_a_single_class_section()
    {
        $this->assertCount(
            1,
            $this->parseFile($this->druidSpellsPath)
        );
    }

    private function parseFile(string $path)
    {
        return (new ClassSectionParser())->parse(
            $this->loadRaw($path)
        );
    }

    /** @test */
    public function it_finds_the_expected_class()
    {
        $sections = $this->parseFile($this->druidSpellsPath);
        $section = $sections[0];
        $this->assertEquals(
            $section->getSpellCasterClass(),
            'DRUID'
        );
    }

    /** @test */
    public function it_contains_the_expected_content()
    {
        $sections = $this->parseFile($this->druidSpellsPath);
        $section = $sections[0];
        $this->assertStringStartsWith(
            "The religious symbol of druids is mistletoe. Of lesser importance is holly.",
            $section->getSection()
        );
        $this->assertStringEndsWith(
            "are mistletoe and a piece of food attractive to the animal subject.\n",
            $section->getSection()
        );
    }

    /** @test */
    public function it_finds_all_expected_classes()
    {
        $sections = $this->parseFile($this->allClassesPath);
        $this->assertEquals(
            ['CLERIC', 'DRUID', 'MAGIC-USER', 'ILLUSIONIST'],
            array_map(fn (ClassSection $section) => $section->getSpellCasterClass(), $sections)
        );
    }

    /** @test */
    public function it_finds_all_expected_contents()
    {
        $sections = $this->parseFile($this->allClassesPath);
        $this->assertEquals(
            [
                'All material components required for the var',
                'The religious symbol of druids is mistletoe.',
                'Magic-users employ a greater variety of mate',
                'There are fewer illusionist spells than ther'
            ],
            array_map(
                fn (ClassSection $section) => mb_substr($section->getSection(), 0, 44),
                $sections
            ),
            'Failed asserting that the sections start as expected'
        );
        $this->assertEquals(
            [
                " this spell are fire and holy/unholy water.\n",
                "to wood would be forevermore a wooden door.\n",
                "the wishing character out of the campaign.)\n",
                "el magic-user spell, audible glamer (q.v.).\n"
            ],
            array_map(
                fn (ClassSection $section) => mb_substr($section->getSection(), -44),
                $sections
            ),
            'Failed asserting that the sections end as expected'
        );
    }
}
