<?php

namespace Tests\SpellParser\Spells;

use SpellParser\SpellParser\Spell\SpellSection;
use SpellParser\SpellParser\InputCleaner;
use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spells\SpellsParser;
use Tests\TestCase;

class SpellsParserTest extends TestCase
{
    /**
     * @test
     */
    public function it_finds_a_spell_when_it_is_the_only_content()
    {
        $spells = $this->parseFile('1-spell.txt');
        $this->assertCount(1, $spells);
    }

    /**
     * @test
     */
    public function it_finds_multiple_spells()
    {
        $spells = $this->parseFile('3-spells.txt');
        $this->assertCount(3, $spells);
    }

    /**
     * @test
     */
    public function it_finds_multiple_spells_if_the_attributes_contain_newlines()
    {
        $spells = $this->parseFile('2-spells-newlines-in-attribute-section.txt');
        $this->assertCount(2, $spells);
    }

    /**
     * @test
     */
    public function it_finds_multiple_spells_with_input_distortion()
    {
        $raw = $this->loadRaw('8-distorted-spells.txt');
        $cleanedUp = (new InputCleaner($raw))->cleanup();
        $spells = $this->parseString($cleanedUp);
        $this->assertCount(8, $spells);
    }
    /**
     * @test
     */
    public function it_starts_with_the_expected_heading()
    {
        $spells = $this->parseFile('1-spell.txt');
        $this->assertStringStartsWith('Bless (Conjuration/Summoning) Reversible', $spells[0]->getPlainString());
    }

    /**
     * @test
     */
    public function it_starts_with_the_expected_headings_for_multiple_spells()
    {
        $spells = $this->parseFile('3-spells.txt');
        $expectations = [
            'Command (Enchantment/Charm)' => $spells[0]->getPlainString(),
            'Create Water (Alteration) Reversible' => $spells[1]->getPlainString(),
            'Cure Light Wounds (Necromantic) Reversible' => $spells[2]->getPlainString()
        ];
        foreach ($expectations as $expectation => $parsedValue) {
            $this->assertStringStartsWith($expectation, $parsedValue);
        }
    }

    /**
     * @test
     */
    public function it_starts_with_the_expected_headings_even_with_preceding_clutter()
    {
        $raw = $this->loadRaw('3-spells.txt');
        $clutter = "referee in this regard and ask his ruling and reasoning.\nFirst Level Spells:";
        $spells = $this->parseString("${clutter}\n${raw}");
        $expectations = [
            'Command (Enchantment/Charm)' => $spells[0]->getPlainString(),
            'Create Water (Alteration) Reversible' => $spells[1]->getPlainString(),
            'Cure Light Wounds (Necromantic) Reversible' => $spells[2]->getPlainString()
        ];
        foreach ($expectations as $expectation => $parsedValue) {
            $this->assertStringStartsWith($expectation, $parsedValue);
        }
    }

    /**
     * @test
     */
    public function it_contains_the_complete_content_for_a_single_spell()
    {
        $spells = $this->parseFile('1-spell.txt');
        $expected = "the bless requires holy water, while the curse requires the sprinkling of specially\npolluted water.\n";
        $this->assertStringEndsWith($expected, $spells[0]->getPlainString());
    }

    /**
     * @test
     */
    public function it_captures_the_complete_spell_when_parsing_multiple_spells()
    {
        $spells = $this->parseFile('3-spells.txt');
        $expectations = [
            "intelligence and 6 hit dice/levels do not get 2 saving throws!)\n" => $spells[0]->getPlainString(),
            "dust to destroy, water. Note that water cannot be created within a living thing.\n" => $spells[1]->getPlainString(),
            "will. Caused light wounds are 1 to 8 hit points of damage.\n" => $spells[2]->getPlainString()
        ];
        foreach ($expectations as $expectation => $parsedValue) {
            $this->assertStringEndsWith($expectation, $parsedValue);
        }
    }

    /**
     * @test
     */
    public function it_doesnt_capture_the_succeeding_heading()
    {
        $raw = $this->loadRaw('3-spells.txt');
        $heading = "MAGIC-USER SPELLS\nNotes Regarding Magic-User Spells::\nMagic-users employ a greater variety of material components than do other";
        $cluttered = "${raw}${heading}\n";
        $spells = $this->parseString($cluttered);
        $this->assertStringEndsWith(
            "will. Caused light wounds are 1 to 8 hit points of damage.\n",
            $spells[2]->getPlainString()
        );
    }

    /**
     * @return SpellSection[]
     */
    private function parseFile(string $file): array
    {
        return $this->parseString($this->loadRaw($file));
    }

    /**
     * @return SpellSection[]
     */
    private function parseString(string $string): array
    {
        return (new SpellsParser($string))->parse(new Factory());
    }
}
