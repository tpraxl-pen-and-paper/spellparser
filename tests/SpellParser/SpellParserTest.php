<?php

namespace Tests\SpellParser;

use Exception;
use SpellParser\SpellParser\Spell\Heading\SpellHeadingParseException;
use SpellParser\SpellParser\Spell\SpellPatterns;
use SpellParser\SpellParser\SpellParser;
use Tests\TestCase;

class SpellParserTest extends TestCase
{
    /** @test */
    public function it_creates_as_many_renderers_as_there_are_spells()
    {
        $renderers = (new SpellParser($this->loadRaw("3-spells.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertCount(3, $renderers);
    }

    /** @test */
    public function it_produces_the_expected_renderers()
    {
        $renderers = (new SpellParser($this->loadRaw("3-spells.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertEquals(
            $this->loadCsvAsArray("3-spells.csv"),
            array_map(fn ($renderer) => $renderer->renderArray(), $renderers)
        );
    }

    private function loadCsvAsArray(string $pathRelativeToFixtureFolder): array
    {
        $fileHandle = fopen(__DIR__."/../fixtures/${pathRelativeToFixtureFolder}", 'r');
        $result = [];
        while (($row = fgetcsv($fileHandle)) !== false) {
            $result [] = $row;
        }
        fclose($fileHandle);
        return $result;
    }

    /** @test */
    public function it_filters_out_distortions()
    {
        $renderers = (new SpellParser($this->loadRaw("8-distorted-spells.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertCount(8, $renderers);
    }

    /** @test */
    public function it_throws_an_exception_if_the_header_cannot_be_parsed()
    {
        try {
            (new SpellParser($this->loadRaw("broken-heading.txt")))->parseToCsvRowRenderers(
                '.+', // <- using the simple heading (sub) pattern to provoke an exception
            );
            $this->fail("Failed expecting a thrown Exception for a broken heading");
        } catch (SpellHeadingParseException $e) {
            $this->assertStringContainsString('Failed to parse', $e->getMessage());
        } catch (Exception $e) {
            $this->fail("Unexpected Exception of type " . get_class($e) . ". Expected " . SpellHeadingParseException::class);
        }
    }

    /** @test */
    public function it_uses_the_given_exception_handler_if_present()
    {
        $exception = null;
        (new SpellParser($this->loadRaw("broken-heading.txt")))
            ->parseToCsvRowRenderers(
                '.+', // <- using the simple heading (sub) pattern to provoke an exception,
                function (SpellHeadingParseException $e) use (&$exception) {
                    $exception = $e;
                }
            );
        $this->assertNotNull($exception);
        $this->assertStringContainsString('Failed to parse', $exception->getMessage());
    }

    /** @test */
    public function it_returns_the_return_value_of_the_given_exception_handler()
    {
        $expectedReturnValue = "abc";
        $result = (new SpellParser($this->loadRaw("broken-heading.txt")))
            ->parseToCsvRowRenderers(
                '.+', // <- using the simple heading (sub) pattern to provoke an exception
                fn (SpellHeadingParseException $e) => $expectedReturnValue
            );
        $this->assertNotEmpty($result);
        $this->assertEquals(
            $expectedReturnValue,
            $result[0],
            'Failed asserting that the exception handler has been called and returned the expected value'
        );
    }

    /** @test */
    public function it_recognizes_a_spell_even_if_there_is_a_misleading_preceding_section()
    {
        // Former bug: misleading double colon suffix in subheading
        // "Notes Regarding Magic-User Spells::"
        // Prevented the recognition of "Affect Normal Fires" spell
        $renderers = (new SpellParser($this->loadRaw("misleading-heading/affect-normal-fires.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertCount(1, $renderers);
    }

    /** @test */
    public function it_recognizes_a_spell_even_if_there_is_a_misleading_heading()
    {
        $renderers = (new SpellParser($this->loadRaw("misleading-heading/polymorph-self.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertCount(1, $renderers);
    }

    /** @test */
    public function it_correctly_detects_the_spell_title()
    {
        // concerning the bug where "cure serious wounds" and "fire seeds" would have been prefixed with unwanted text
        $renderers = (new SpellParser($this->loadRaw("misleading-heading/cure-serious-wounds.txt")))
            ->parseToCsvRowRenderers((new SpellPatterns())->heading());
        $this->assertCount(1, $renderers);
        $this->assertEquals(
            ['Cure Serious Wounds', 'Necromantic', 'reversible'],
            array_slice($renderers[0]->renderArray(), 1, 3)
        );
    }
}
