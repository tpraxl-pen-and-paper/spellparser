<?php

namespace Tests\SpellParser;

use SpellParser\SpellParser\InputCleaner;
use Tests\TestCase;

class InputCleanerTest extends TestCase
{
    /**
     * @test
     */
    public function it_leaves_no_page_distortions()
    {
        $raw = $this->loadRaw('8-distorted-spells.txt');
        $cleanedUp = (new InputCleaner($raw))->cleanup();
        $unexpectedStrings = [
            '70',
            'MAGIC-USER SPELLS (2ND LEVEL) MAGIC-USER SPELLS (2ND LEVEL)',
            '71',
            'MAGIC-USER SPELLS (2ND LEVEL) MAGIC-USER SPELLS (3RD LEVEL)'
        ];
        foreach ($unexpectedStrings as $unexpected) {
            $this->assertStringNotContainsString($unexpected, $cleanedUp);
            $this->assertStringContainsString($unexpected, $raw);
        }
    }
}
