<?php

namespace Tests\Cli;

use SpellParser\Cli\Format;
use SpellParser\Cli\FormatCode;
use Tests\TestCase;

class FormatTest extends TestCase
{
    /** @test */
    public function it_simply_inserts_any_string_as_color()
    {
        $anyStringAsColorCode = '<color>';
        $this->assertEquals(
            "\033[${anyStringAsColorCode}moutput\033[0m",
            (new Format())->colored("output", $anyStringAsColorCode)
        );
    }

    /** @test */
    public function it_preserves_the_output()
    {
        $output = "output";
        $this->assertStringContainsString(
            $output,
            (new Format())->boldRed($output)
        );
    }

    /** @test */
    public function it_formats_bold_red()
    {
        $this->assertStringContainsString(
            FormatCode::BOLD_RED,
            (new Format())->boldRed("output")
        );
    }

    /** @test */
    public function it_formats_bold_green()
    {
        $this->assertStringContainsString(
            FormatCode::BOLD_GREEN,
            (new Format())->boldGreen("output")
        );
    }
}
