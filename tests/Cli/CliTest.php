<?php

namespace Tests\Cli;

use Closure;
use SpellParser\Cli\Cli;
use SpellParser\Cli\Format;
use Tests\TestCase;

class CliTest extends TestCase
{
    private Cli $cli;
    private string $defaultTargetPath = 'php://temp';
    private string $err = '';

    protected function setUp(): void
    {
        parent::setUp();
        $this->err = '';
        $this->cli = $this->makeCli();
        ob_start();
    }

    private function makeCli(string $singleHeadingPattern = null): Cli
    {
        return (new Cli(
            new Format(),
            $this->defaultTargetPath,
            $singleHeadingPattern,
            function (string $errOutput) {
                $this->err .= $errOutput;
            },
            function (int $code) {
            }
        ));
    }

    protected function tearDown(): void
    {
        ob_end_clean();
        parent::tearDown();
    }


    /** @test */
    public function it_outputs_usage_information_if_src_path_is_missing()
    {
        $this->assertCallOutput(
            [],
            'Usage'
        );
    }

    private function assertCallOutput(array $args, string ...$expectedStrings): void
    {
        $this->assertCallOutputContainsAll(
            $args,
            fn () => ob_get_contents(),
            ...$expectedStrings
        );
    }

    private function assertCallOutputContainsAll(array $args, Closure $outputReceiver, string ...$expectedStrings)
    {
        $this->cli->main(['main.php', ...$args]);
        $output = $outputReceiver();
        foreach ($expectedStrings as $expected) {
            $this->assertStringContainsString(
                $expected,
                $output
            );
        }
    }

    /** @test */
    public function it_uses_the_default_target_path_if_not_given()
    {
        $this->assertCallOutput(
            [__DIR__ . '/../fixtures/3-spells.txt'],
            $this->defaultTargetPath
        );
    }

    /** @test */
    public function it_uses_the_given_target_path()
    {
        $maxMBAndTestId = '1234567';
        $target = "php://temp/maxmemory:${maxMBAndTestId}";
        $this->assertCallOutput(
            [__DIR__ . '/../fixtures/3-spells.txt', $target],
            $target
        );
    }

    /**
     * TODO: Fix this bug
     * @test
     */
    public function it_doesnt_handle_invalid_src_files()
    {
        $this->assertCallOutput(
            ['non-existing-src-file'],
            // is empty, because the following warning is converted
            // into an exception during test runtime (see phpunit.xml.dist)
            // file_get_contents(non-existing-src-file): Failed to open stream: No such file or directory
            //
            // The Exception gets caught in main and triggers an STDERR output,
            // but essentially, the program behaves differently under test
            ''
        );
    }

    /** @test */
    public function it_reports_parse_problems()
    {
        $this->cli = $this->makeCli(
            '.+' // <- using this pattern to provoke a parse problem
        );
        $this->assertCallErrOutput(
            [__DIR__ . '/../fixtures/broken-heading.txt'],
            'FAILED',
            'Bless broken-heading Reversible',
        );
    }

    private function assertCallErrOutput(array $args, string ...$expectedStrings)
    {
        $this->assertCallOutputContainsAll(
            $args,
            fn () => $this->err,
            ...$expectedStrings
        );
    }
}
