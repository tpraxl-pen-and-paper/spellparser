<?php

namespace Tests;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function loadRaw(string $string): string
    {
        return file_get_contents(__DIR__ . "/fixtures/${string}");
    }
}
