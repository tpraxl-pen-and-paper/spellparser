<?php

namespace SpellParser\SpellParser;

class InputCleaner
{
    private string $input;

    public function __construct(string $input)
    {
        $this->input = $input;
    }

    public function cleanup(): string
    {
        $pageNumber = '(?<=\n)[0-9]+\n(?=[A-Z]+)';
        $pageTitle = '[^\n]+\n*';
        return preg_replace('#' . $pageNumber . $pageTitle . '#u', '', $this->input);
    }
}
