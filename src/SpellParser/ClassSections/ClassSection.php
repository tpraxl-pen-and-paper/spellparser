<?php

namespace SpellParser\SpellParser\ClassSections;

class ClassSection
{
    private string $spellCasterClass;
    private string $section;

    public function __construct(string $spellCasterClass, string $section)
    {
        $this->spellCasterClass = $spellCasterClass;
        $this->section = $section;
    }

    public function getSpellCasterClass(): string
    {
        return $this->spellCasterClass;
    }

    public function getSection(): string
    {
        return $this->section;
    }
}
