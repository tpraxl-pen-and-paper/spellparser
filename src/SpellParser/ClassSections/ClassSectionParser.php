<?php

namespace SpellParser\SpellParser\ClassSections;

class ClassSectionParser
{
    public function parse(string $input)
    {
        $classHeading = $this->getClassHeadingPattern("class");
        $limit = join('|', ['$', $this->getClassHeadingPattern()]);
        $matches = [];
        preg_match_all(
            "#\n?${classHeading}\n(?<section>(?:.+\n)+?)(?=${limit})#u",
            $input,
            $matches,
            PREG_SET_ORDER
        );
        return array_map(
            fn ($match) => new ClassSection($match['class'], $match['section']),
            $matches
        );
    }

    private function getClassHeadingPattern(string $capturingGroupName = null): string
    {
        $capturingGroupNameClassifier = $capturingGroupName ? "<${capturingGroupName}>" : ':';
        return "(?${capturingGroupNameClassifier}[A-Z\-\s]+)\sSPELLS\nNotes Regarding .+ Spells:{1,2}";
    }
}
