<?php

namespace SpellParser\SpellParser\Spells;

use SpellParser\SpellParser\Spell\SpellSection;
use SpellParser\SpellParser\SearchPattern\Factory;

class SpellsParser
{
    private string $plainText;

    public function __construct(string $plainText)
    {
        $this->plainText = $plainText;
    }

    /**
     * @return SpellSection[]
     */
    public function parse(Factory $factory, string $spellHeading = '.+'): array
    {
        $matches = [];
        preg_match_all(
            $this->getRegularExpression($factory, $spellHeading),
            $this->plainText,
            $matches,
            PREG_SET_ORDER
        );
        return array_map(
            fn ($match) => new SpellSection($match["heading"], $match["attributes"], $match["explanation"]),
            $matches
        );
    }

    private function getRegularExpression(Factory $factory, string $spellHeading): string
    {
        return $factory->makeExpression(
            $this->getSearchFragments(new SpellsPatterns(), $spellHeading)
        );
    }
    private function getSearchFragments(SpellsPatterns $patterns, string $spellHeadingPattern): array
    {
        return [
                $patterns->heading($spellHeadingPattern),
                $patterns->attributes(),
                $patterns->explanation($patterns->limits())
            ];
    }
}
