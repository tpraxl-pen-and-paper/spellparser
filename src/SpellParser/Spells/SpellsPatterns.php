<?php

namespace SpellParser\SpellParser\Spells;

class SpellsPatterns
{
    /**
     * expect a line, followed by a line with characters, colon and characters.
     * Don't match the following line, use a positive lookahead (?=).
     *
     * Instead of simply matching a line, you can also provide a
     * sub regular expression to be more specific on the
     * expected content of the line, thus prevent
     * wrong recognitions.
     *
     * Example:
     * heading heading
     * Level: 1
     */
    public function heading(string $spellHeading = '.+'): string
    {
        return "(?<heading>${spellHeading}\\n(?=.+:.+\\n))";
    }

    /**
     * expect a multiple occurrences of lines with characters, colon, then characters.
     * Don't match the following line, use a positive lookahead (?=).
     *
     * Example:
     * Level: 1
     * Components: V, S, M
     */
    public function attributes(): string
    {
        return '(?<attributes>(.+\n)+?)(?=Explanation/Description)';
    }

    /**
     * Match the description – between the prefix and the possible section limits
     * @param string $limitFragment
     * @return string
     */
    public function explanation(string $limitFragment): string
    {
        return 'Explanation/Description: (?<explanation>(?:.+\n)+?)' . $limitFragment;
    }

    /**
     * Doesn't match anything: A positive lookahead that determines the limit
     * of a spell's description section.
     */
    public function limits(): string
    {
        $headOfNextSpell = '.+\nLevel:';
        $endOfInput = '$';
        $uppercaseFollowupHeading = '[A-Z]{3,}.+\n';
        $limits = join(
            '|',
            [
                $headOfNextSpell,
                $endOfInput,
                $uppercaseFollowupHeading
            ]
        );
        return '(?=(?:' . $limits . '))';
    }
}
