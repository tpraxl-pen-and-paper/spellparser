<?php

namespace SpellParser\SpellParser\SearchPattern;

class Factory
{
    public function makeExpression(array $fragments, string $modifier = 'u', string $delimiter = '#'): string
    {
        $fragment = join('', $fragments);
        return $delimiter . $fragment . $delimiter . $modifier;
    }
}
