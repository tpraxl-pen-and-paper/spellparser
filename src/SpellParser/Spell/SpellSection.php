<?php

namespace SpellParser\SpellParser\Spell;

class SpellSection
{
    private string $heading;
    private string $attributes;
    private string $explanation;

    public function __construct(string $heading, string $attributes, string $explanation)
    {
        $this->heading = $heading;
        $this->attributes = $attributes;
        $this->explanation = $explanation;
    }

    public function getPlainString(): string
    {
        return join('', [
            $this->getHeading(),
            $this->getAttributesSection(),
            'Explanation/Description: ',
            $this->getExplanation()
        ]);
    }

    public function getHeading(): string
    {
        return $this->heading;
    }

    public function getAttributesSection(): string
    {
        return $this->attributes;
    }

    public function getExplanation(): string
    {
        return $this->explanation;
    }
}
