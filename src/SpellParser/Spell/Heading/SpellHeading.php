<?php

namespace SpellParser\SpellParser\Spell\Heading;

class SpellHeading
{
    private string $title;
    private string $school;
    private bool $reversible;

    public function __construct(string $title, string $school, bool $reversible)
    {
        $this->title = $title;
        $this->school = $school;
        $this->reversible = $reversible;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSchool(): string
    {
        return $this->school;
    }

    public function isReversible(): bool
    {
        return $this->reversible;
    }

    /**
     * @return array associative array with keys "title", "school", "reversible"
     */
    public function toArray(): array
    {
        return [
            'title' => $this->getTitle(),
            'school' => $this->getSchool(),
            'reversible' => $this->isReversible() ? 'reversible' : '',
        ];
    }
}
