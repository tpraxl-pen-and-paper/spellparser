<?php

namespace SpellParser\SpellParser\Spell\Heading;

use RuntimeException;

class SpellHeadingParseException extends RuntimeException
{
}
