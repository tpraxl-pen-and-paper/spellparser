<?php

namespace SpellParser\SpellParser\Spell\Heading;

use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spell\SpellPatterns;

class SpellHeadingParser
{
    private string $heading;

    public function __construct(string $heading)
    {
        $this->heading = $heading;
    }

    /**
     * @param Factory $factory
     * @return SpellHeading
     * @throws SpellHeadingParseException if the heading could not be parsed to a result
     */
    public function parseHeading(Factory $factory): SpellHeading
    {
        $matches = [];
        $result = preg_match(
            $this->getRegularExpression($factory),
            $this->heading,
            $matches,
            0
        );
        if (!$result) {
            $expression = $this->getRegularExpression($factory);
            throw new SpellHeadingParseException("Failed to parse the following string: '$this->heading' with '${expression}'");
        }
        return new SpellHeading($matches['title'], $matches['school'], isset($matches['reversible']));
    }

    private function getRegularExpression(Factory $factory): string
    {
        return $factory->makeExpression(
            [ (new SpellPatterns())->heading() ]
        );
    }
}
