<?php

namespace SpellParser\SpellParser\Spell;

use SpellParser\SpellParser\Spell\Attributes\SpellAttributeNames;

class SpellPatterns
{
    public function heading(): string
    {
        $startOfLineOrInput = '(?<=^|\n)';
        return $startOfLineOrInput . '(?<title>.+)\s+\((?<school>.+)\)\s*(?<reversible>Reversible)?';
    }

    public function attributes(): string
    {
        $spellAttributeNames = join('|', SpellAttributeNames::SPELL_ATTRIBUTE_NAMES);
        return "(${spellAttributeNames}):([^:]+)(?=${spellAttributeNames}|\$)";
    }
}
