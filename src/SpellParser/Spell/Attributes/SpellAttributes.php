<?php

namespace SpellParser\SpellParser\Spell\Attributes;

class SpellAttributes
{
    private ?string $level;
    private ?string $components;
    private ?string $range;
    private ?string $castingTime;
    private ?string $duration;
    private ?string $savingThrow;
    private ?string $areaOfEffect;

    public function __construct(?string $level, ?string $components, ?string $range, ?string $castingTime, ?string $duration, ?string $savingThrow, ?string $areaOfEffect)
    {
        $this->level = $level;
        $this->components = $components;
        $this->range = $range;
        $this->castingTime = $castingTime;
        $this->duration = $duration;
        $this->savingThrow = $savingThrow;
        $this->areaOfEffect = $areaOfEffect;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function getComponents(): ?string
    {
        return $this->components;
    }

    public function getRange(): ?string
    {
        return $this->range;
    }

    public function getCastingTime(): ?string
    {
        return $this->castingTime;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function getSavingThrow(): ?string
    {
        return $this->savingThrow;
    }

    public function getAreaOfEffect(): ?string
    {
        return $this->areaOfEffect;
    }

    /**
     * @return array associative array with keys "level", "components",
     * "range", "casting-time", "duration", "saving-throw",
     * "area-of-effect"
     */
    public function toArray(): array
    {
        return [
            SpellAttributeNames::LEVEL => $this->getLevel(),
            SpellAttributeNames::COMPONENTS => $this->getComponents(),
            SpellAttributeNames::RANGE => $this->getRange(),
            SpellAttributeNames::CASTING_TIME => $this->getCastingTime(),
            SpellAttributeNames::DURATION => $this->getDuration(),
            SpellAttributeNames::SAVING_THROW => $this->getSavingThrow(),
            SpellAttributeNames::AREA_OF_EFFECT => $this->getAreaOfEffect()
        ];
    }
}
