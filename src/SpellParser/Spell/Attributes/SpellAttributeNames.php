<?php

namespace SpellParser\SpellParser\Spell\Attributes;

class SpellAttributeNames
{
    public const LEVEL = 'Level';
    public const COMPONENTS = 'Components';
    public const RANGE = 'Range';
    public const CASTING_TIME = 'Casting Time';
    public const DURATION = 'Duration';
    public const SAVING_THROW = 'Saving Throw';
    public const AREA_OF_EFFECT = 'Area of Effect';
    public const SPELL_ATTRIBUTE_NAMES = [
        self::LEVEL,
        self::COMPONENTS,
        self::RANGE,
        self::CASTING_TIME,
        self::DURATION,
        self::SAVING_THROW,
        self::AREA_OF_EFFECT
    ];
}
