<?php

namespace SpellParser\SpellParser\Spell\Attributes;

use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spell\SpellPatterns;

class SpellAttributesParser
{
    private string $attributesSection;

    public function __construct(string $attributesSection)
    {
        $this->attributesSection = $attributesSection;
    }

    public function parseAttributes(Factory $factory): SpellAttributes
    {
        $matches = [];
        preg_match_all(
            $this->getRegularExpression($factory),
            $this->attributesSection,
            $matches,
            0
        );
        $attributes = array_combine(
            $matches[1],
            array_map(fn ($match) => trim($match), $matches[2])
        );
        return new SpellAttributes(
            $attributes[SpellAttributeNames::LEVEL] ?? null,
            $attributes[SpellAttributeNames::COMPONENTS] ?? null,
            $attributes[SpellAttributeNames::RANGE] ?? null,
            $attributes[SpellAttributeNames::CASTING_TIME] ?? null,
            $attributes[SpellAttributeNames::DURATION] ?? null,
            $attributes[SpellAttributeNames::SAVING_THROW] ?? null,
            $attributes[SpellAttributeNames::AREA_OF_EFFECT] ?? null
        );
    }

    private function getRegularExpression(Factory $factory): string
    {
        return $factory->makeExpression(
            [ (new SpellPatterns())->attributes() ]
        );
    }
}
