<?php

namespace SpellParser\SpellParser;

use SpellParser\Csv\SpellRowRenderer;
use SpellParser\SpellParser\ClassSections\ClassSection;
use SpellParser\SpellParser\ClassSections\ClassSectionParser;
use SpellParser\SpellParser\SearchPattern\Factory;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributes;
use SpellParser\SpellParser\Spell\Attributes\SpellAttributesParser;
use SpellParser\SpellParser\Spell\Heading\SpellHeading;
use SpellParser\SpellParser\Spell\Heading\SpellHeadingParseException;
use SpellParser\SpellParser\Spell\Heading\SpellHeadingParser;
use SpellParser\SpellParser\Spell\SpellSection;
use SpellParser\SpellParser\Spells\SpellsParser;

class SpellParser
{
    private string $rawInput;

    /**
     * @param string $rawInput
     */
    public function __construct(string $rawInput)
    {
        $this->rawInput = $rawInput;
    }

    /**
     * @param callable|null $handleException
     * @return SpellRowRenderer[]
     */
    public function parseToCsvRowRenderers(string $spellHeadingPattern = '.+', callable $handleException = null): array
    {
        $cleansedInput = $this->cleanInput($this->rawInput);
        $classSections = (new ClassSectionParser())->parse($cleansedInput);
        if (empty($classSections)) {
            $classSections = [new ClassSection('no class', $cleansedInput)];
        }
        $spellRenderers = [];
        foreach ($classSections as $classSection) {
            $spellSections = $this->parseForSpellSections($classSection->getSection(), $spellHeadingPattern);
            $spellRenderers = [...$spellRenderers, ...$this->parseSpells($spellSections, $classSection->getSpellCasterClass(), $handleException)];
        }
        return $this->filterNonNullSpellRenderers($spellRenderers);
    }

    private function cleanInput(string $rawInput): string
    {
        return (new InputCleaner($rawInput))->cleanup();
    }

    /**
     * @param string $cleansedInput
     * @return SpellSection[]
     */
    private function parseForSpellSections(string $cleansedInput, string $spellHeading): array
    {
        return (new SpellsParser($cleansedInput))->parse(new Factory(), $spellHeading);
    }

    /**
     * @param SpellSection[] $spellSections
     * @param callable|null $handleException
     * @return SpellRowRenderer[]
     */
    private function parseSpells(array $spellSections, string $casterClass, callable $handleException = null): array
    {
        return array_map(function (SpellSection $spellSection) use ($casterClass, $handleException) {
            try {
                return new SpellRowRenderer(
                    $casterClass,
                    $this->parseHeading($spellSection),
                    $this->parseAttributes($spellSection),
                    $spellSection->getExplanation()
                );
            } catch (SpellHeadingParseException $e) {
                if (!$handleException) {
                    throw $e;
                }
                return $handleException($e, $spellSection);
            }
        }, $spellSections);
    }

    /**
     * @param SpellRowRenderer[] $spellRenderers
     * @return SpellRowRenderer[]
     */
    private function filterNonNullSpellRenderers(array $spellRenderers): array
    {
        return array_filter($spellRenderers);
    }

    private function parseHeading(SpellSection $spellSection): SpellHeading
    {
        return (new SpellHeadingParser($spellSection->getHeading()))->parseHeading(new Factory());
    }

    private function parseAttributes(SpellSection $spellSection): SpellAttributes
    {
        return (new SpellAttributesParser($spellSection->getAttributesSection()))->parseAttributes(new Factory());
    }
}
