<?php

namespace SpellParser\Csv;

class Csv
{
    public function toFile(string $pathToExistingFile, callable $contentRetriever)
    {
        $csv = $this->openAndWrite($pathToExistingFile, $contentRetriever);
        fclose($csv);
    }

    public function toString(callable $contentRetriever)
    {
        // taken from https://www.php.net/manual/en/function.fputcsv.php#74118
        // output up to 5MB is kept in memory, if it becomes bigger it will automatically be written to a temporary file
        $csv = $this->openAndWrite(
            'php://temp/maxmemory:'. (5*1024*1024),
            $contentRetriever
        );
        rewind($csv);
        $result = stream_get_contents($csv);
        fclose($csv);
        return $result;
    }

    private function openAndWrite($filename, callable $contentRetriever)
    {
        $csv = fopen($filename, 'r+');
        $contentRetriever(new LineWriter($csv));
        return $csv;
    }
}
