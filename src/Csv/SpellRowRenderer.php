<?php

namespace SpellParser\Csv;

use SpellParser\SpellParser\Spell\Attributes\SpellAttributes;
use SpellParser\SpellParser\Spell\Heading\SpellHeading;

class SpellRowRenderer
{
    private SpellHeading $heading;
    private SpellAttributes $attributes;
    private string $explanation;
    private string $casterClass;

    public function __construct(string $casterClass, SpellHeading $heading, SpellAttributes $attributes, string $explanation)
    {
        $this->casterClass = $casterClass;
        $this->heading = $heading;
        $this->attributes = $attributes;
        $this->explanation = $explanation;
    }

    public function renderArray(): array
    {
        return [
            $this->casterClass,
            ...array_values($this->heading->toArray()),
            ...array_values($this->attributes->toArray()),
            $this->explanation
        ];
    }
}
