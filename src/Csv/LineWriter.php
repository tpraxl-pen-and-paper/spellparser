<?php

namespace SpellParser\Csv;

class LineWriter
{
    /**
     * @var resource $stream
     */
    private $stream;

    /**
     * @param resource $stream
     */
    public function __construct($stream)
    {
        $this->stream = $stream;
    }

    public function append(array $array): void
    {
        fputcsv($this->stream, $array);
    }
}
