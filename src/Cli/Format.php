<?php

namespace SpellParser\Cli;

class Format
{
    public function boldRed(string $output): string
    {
        return $this->colored($output, FormatCode::BOLD_RED);
    }

    public function colored(string $output, string $color): string
    {
        return "\033[${color}m${output}\033[0m";
    }

    public function boldGreen(string $output): string
    {
        return $this->colored($output, FormatCode::BOLD_GREEN);
    }

    public function boldYellow(string $output): string
    {
        return $this->colored($output, FormatCode::BOLD_YELLOW);
    }
}
