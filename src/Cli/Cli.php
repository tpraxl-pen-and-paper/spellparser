<?php

namespace SpellParser\Cli;

use Closure;
use Exception;
use SpellParser\Csv\Csv;
use SpellParser\Csv\LineWriter;
use SpellParser\SpellParser\Spell\SpellPatterns;
use SpellParser\SpellParser\Spell\SpellSection;
use SpellParser\SpellParser\SpellParser;

class Cli
{
    private Format $format;
    /**
     * @var callable|Closure
     */
    private $exitHandler;
    private string $defaultTargetPath;
    /**
     * @var callable|Closure
     */
    private $writeToStdErr;
    private string $singleHeadingPattern;

    public function __construct(
        Format $format,
        string $defaultTargetPath = 'test-result.csv',
        string $singleHeadingPattern = null,
        callable $writeToStdErr = null,
        callable $handleExit = null
    ) {
        $this->format = $format;
        $this->defaultTargetPath = $defaultTargetPath;
        $this->singleHeadingPattern = $singleHeadingPattern ?? (new SpellPatterns())->heading();
        $this->writeToStdErr = $writeToStdErr ?? function (string $output) {
            fwrite(STDERR, $output . PHP_EOL);
        };
        $this->exitHandler = $handleExit ?? function ($code) {
            exit($code);
        };
    }

    public function main($args)
    {
        try {
            $srcPath = $this->getPathToTxt($args);
            $targetPath = $this->getTargetPath($args);
            $this->parseAndSave($srcPath, $targetPath);
        } catch (UsageException $e) {
            $this->cliPrintErr($this->format->boldRed($e->getMessage()));
            echo "Usage:\nphp main.php <path-to-txt-file> [<path-to-target-csv-file>]\n";
            $this->exit(1);
        } catch (Exception $e) {
            $this->cliPrintErr($this->format->boldRed($e->getMessage()));
            $this->exit(1);
        }
    }

    private function getPathToTxt($argv): string
    {
        if (isset($argv[1])) {
            return $argv[1];
        }
        throw new UsageException("Missing path to TXT file to import from.");
    }

    private function getTargetPath($argv): string
    {
        if (isset($argv[2])) {
            return $argv[2];
        }
        return $this->defaultTargetPath;
    }

    public function parseAndSave(string $srcPath, string $targetPath)
    {
        $cleansedSpellRenderers = $this->parseForSpells($srcPath);
        $csvString = $this->renderCsvString($cleansedSpellRenderers);
        $this->printParsedSpellsAmount(count($cleansedSpellRenderers));
        $this->saveToFile($targetPath, $csvString);
        $this->printSuccessfullySaved($targetPath);
    }

    /**
     * @return SpellParser[]
     */
    private function parseForSpells(string $srcPath): array
    {
        return (new SpellParser(file_get_contents($srcPath)))
            ->parseToCsvRowRenderers(
                $this->singleHeadingPattern,
                fn ($e, $spellSection) => $this->handleParseException($e, $spellSection)
            );
    }

    private function handleParseException(Exception $e, SpellSection $spellSection)
    {
        $this->cliPrintErr($this->format->boldRed($e->getMessage()));
        $section = $this->format->boldYellow($spellSection->getPlainString());
        $this->cliPrintErr("\nFAILED:»\n{$section}«\n\n");
        return null;
    }

    private function renderCsvString(array $cleansedSpellRenderers): string
    {
        $csv = new Csv();
        return $csv->toString(function (LineWriter $lineWriter) use ($cleansedSpellRenderers) {
            foreach ($cleansedSpellRenderers as $spell) {
                $lineWriter->append($spell->renderArray());
            }
        });
    }

    private function printParsedSpellsAmount(int $amountParsed)
    {
        echo $this->format->boldGreen("Successfully parsed ${amountParsed} spells.\n");
    }

    private function saveToFile(string $targetPath, string $csvString)
    {
        file_put_contents($targetPath, $csvString);
    }

    private function printSuccessfullySaved(string $targetPath)
    {
        echo $this->format->boldGreen("Successfully saved to ${targetPath}\n");
    }

    protected function exit(int $code)
    {
        call_user_func($this->exitHandler, $code);
    }
    protected function cliPrintErr(string $output)
    {
        call_user_func($this->writeToStdErr, $output);
    }
}
