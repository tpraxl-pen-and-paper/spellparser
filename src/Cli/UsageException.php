<?php

namespace SpellParser\Cli;

use RuntimeException;

class UsageException extends RuntimeException
{
}
