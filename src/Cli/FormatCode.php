<?php

namespace SpellParser\Cli;

class FormatCode
{
    public const BOLD_RED = '1;31';
    public const BOLD_GREEN = '1;32';
    public const BOLD_YELLOW = '1;33';
}
