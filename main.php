<?php

use SpellParser\Cli\Cli;
use SpellParser\Cli\Format;

require_once(__DIR__. '/vendor/autoload.php');

(new Cli(new Format()))->main($_SERVER['argv']);
